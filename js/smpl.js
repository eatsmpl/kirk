var currentClock = new Date();
var currentYear = currentClock.getFullYear();
$.getJSON("../mydata.json", function (data) {
//JSON START

//HTML syntax assign
    var syntax = {
        "spanOpen": "<span>",
        "spanClose": "</span>",
        "hr": "<hr>",
        "h3Open": "<h3>",
        "h3Close": "</h3>",
        "pOpen": "<p>",
        "pClose": "</p>",
        "ahrefOpen": "<a href='",
        "ahrefOpenEnd": "'>",
        "ahrefClose": "</a>",
        "pike": {
            "menu": {
                "briefOpen": "<p class='menu-brief'>",
                "briefClose": "</p>",
                "legendOpen": "<p class='legend'>",
                "legendClose": "</p>",
                "legendIndicatorOpen": "<span class='legend-indicator'>",
                "legendIndicatorClose": "</span>",
                "legendIndicatorSpacer": " - ",
                "categoriesOpen": "<p class='menu-category'>",
                "categoriesClose": "</p>"
            },
            "socialIconSpanOpen": "<span class='symbol restaurant-socialIcon ",
            "socialIconSpanOpen2": "'>",
            "socialIconSpanClose": "</span>"
        }
    };
// JSON END

// JS BEGIN
//Schedule Entries
function createScheduleEntries(name) {
    var toReturn = '';
    if (typeof data.restaurant[name] !== 'undefined') {
        for (ind in data.restaurant[name]) {
            toReturn += syntax.pOpen + data.restaurant[name][ind].Day + ": " + data.restaurant[name][ind].openHours + syntax.pClose;
        }
    }
    return toReturn;
}

var scheduleEntries = createScheduleEntries('hours');

//Menu Generator START
//Legend
    function createMenuLegend(name) {
        var toReturn = '';
        if (typeof data.menuUserInput[name] !== 'undefined') {
            for (ind in data.menuUserInput[name]) {
                toReturn += syntax.pike.menu.legendIndicatorOpen + data.menuUserInput[name][ind].symbol + syntax.pike.menu.legendIndicatorClose + syntax.pike.menu.legendIndicatorSpacer + data.menuUserInput[name][ind].description + ", ";
            }
        }
        return toReturn;
    }

//Menu Entries + Categories
    function createMenuEntries(name) {
        var toReturn = '';
        if (typeof data.menuUserInput[name] !== 'undefined') {
            for (ind in data.menuUserInput[name]) {
            toReturn += syntax.pike.menu.categoriesOpen + data.menuUserInput[name][ind].category + syntax.pike.menu.categoriesClose + syntax.h3Open + data.menuUserInput[name][ind].title + " " + syntax.pike.menu.legendIndicatorOpen + data.menuUserInput[name][ind].indicators + syntax.pike.menu.legendIndicatorClose + syntax.h3Close + syntax.pOpen + data.menuUserInput[name][ind].description + syntax.pClose;
            }
        }
        return toReturn;
    }

// Abstracted Menu Items
    var menuItems = {
        "brief": syntax.pike.menu.briefOpen + data.menuUserInput.brief + syntax.pike.menu.briefClose,
        "legend": syntax.pike.menu.legendOpen + createMenuLegend('legend') + syntax.pike.menu.legendClose,
        "entries": createMenuEntries('entries')
    };

// Finished Menu Generation
    var menuComplete = {
        "menu": menuItems.brief + syntax.hr + menuItems.legend + syntax.hr + menuItems.entries
    };

// Social Icons

    function createSocialIcons(name) {
        var toReturn = '';
        if (typeof data.restaurant[name] !== 'undefined') {
            for (ind in data.restaurant[name]) {
                toReturn += syntax.ahrefOpen + data.restaurant[name][ind].link + syntax.ahrefOpenEnd + syntax.pike.socialIconSpanOpen + data.restaurant[name][ind].icon + syntax.pike.socialIconSpanOpen2 + "circle" + data.restaurant[name][ind].icon + syntax.pike.socialIconSpanClose + syntax.ahrefClose;
            }
        }
        return toReturn;
    }

    var socialIcons = createSocialIcons('social');

// Convert everything to HTML
    $(document).ready(function () {
        $('.restaurant-name').html(data.restaurant.name);
        $('.restaurant-slogan').html(data.restaurant.aboutInfo.slogan);
        $('.restaurant-quote').html(data.restaurant.aboutInfo.quote);
        $('.restaurant-quoteCitation').html("-" + data.restaurant.aboutInfo.quoteCitation);
        var numberOfLocations = data.restaurant.address.length;
        var i = 0;
        for (i = 0; i < numberOfLocations; i++) {
        var ADi = '.AD-' + i;
        $( ADi).css("display", "block");
        $( ADi + ' .restaurant-streetAddress').html(data.restaurant.address[i].streetAddress);
        $( ADi + ' .restaurant-city').html(data.restaurant.address[i].city);
        $( ADi + ' .restaurant-state').html(data.restaurant.address[i].state);
        $( ADi + ' .restaurant-postalCode').html(data.restaurant.address[i].postalCode);
        }
        $('.restaurant-phoneNumber').html(data.restaurant.phoneNumber);
        $('.restaurant-contactEmailLink').attr("href", "mailto:" + data.restaurant.contactEmail);
        $('.restaurant-contactEmail').html(data.restaurant.contactEmail);
        $('.hours').html(scheduleEntries);
        $('.restaurant-aboutBio').html(data.restaurant.aboutInfo.aboutBio);
        $('.restaurant-menu').html(menuComplete.menu);
        $('.restaurant-menu').html(menuComplete.menu);
        $('.restaurant-contactForm').attr("action", "////formspree.io/" + data.restaurant.contactEmail);
        $('.restaurant-footer').html("Copyright © " + currentYear + " " + data.restaurant.name + ". All Rights Reserved. / " + "Website designed and hosted by " + "<a href='http://eatsmpl.com'>Eat Smpl</a>");
        $('.restaurant-socialLink').html(null);
        $('.restaurant-socialIcon').html(socialIcons);
        $('.quote-link').click( function() { $('.yelp')[0].click(); return false; } );
    });
});
